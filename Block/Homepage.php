<?php

namespace Kowal\StronaGlowna\Block;

use Magento\Framework\Message\ManagerInterface;

class Homepage extends \Magento\Framework\View\Element\Template
{
    protected $_scopeConfig;
    protected $_manager;

    /**
     * Homepage constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param ManagerInterface $manager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        ManagerInterface $manager
    )
    {

        $this->_scopeConfig = $scopeConfig;
        $this->_manager = $manager;
        parent::__construct($context, $data);
    }


    /**
     * @return int
     */
    public function getColumns($sekcja = 1)
    {
        return (int)$this->getConfig('homepage/sekcja' . $sekcja . '/ilosc_kolumn');
    }

    /**
     * @return int
     */
    public function getSectionStatus($sekcja = 1)
    {
        return (bool)$this->getConfig('homepage/sekcja' . $sekcja . '/enable');
    }

    /**
     * @param $path
     * @return string
     */
    public function getConfig($path)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        return (string)$this->_scopeConfig->getValue($path, $storeScope);
    }

    /**
     * @param $i
     * @return string
     */
    public function getContentBlock($column, $sekcja)
    {
        try {
            $blockId = $this->getConfig('homepage/sekcja' . $sekcja . '/kolumna_' . $column);
            return (string)$this->getLayout()
                ->createBlock('Magento\Cms\Block\Block')
                ->setBlockId($blockId)
                ->toHtml();
        } catch (Exception $e) {
            $this->_manager->addError(__('Błąd odczytu treści blokId: ' . $blockId));
        }
    }

    /**
     * @param $i
     * @return string
     */
    public function getContentHeader($kolumna, $sekcja)
    {
        try {
            $blockId = $this->getConfig('homepage/sekcja' . $sekcja . '/kolumna_' . $kolumna);
            $myCmsBlock = $this->getLayout()
                ->createBlock('Kowal\StronaGlowna\Block\Cms\Block')
                ->setBlockId($blockId);
            return (string)$myCmsBlock->getTitle();
        } catch (Exception $e) {
            $this->_manager->addError(__('Błąd odczytu treści blokId: ' . $blockId));
        }
    }

}
